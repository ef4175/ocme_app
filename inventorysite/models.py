from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.contrib.admin.widgets import AdminDateWidget
import datetime

class Search(models.Model):
	search_term = models.CharField(max_length = 64)
	
	def __str__(self):
		return self.search_term

class Computer(models.Model):
	DELL = "Dell"
	LENOVO = "Lenovo"
	HP = "HP"
	ASUS = "Asus"
	MICROSOFT = "Microsoft"
	ALL_MANUFACTURERS = (
		(DELL, "Dell"),
		(LENOVO, "Lenovo"),
		(HP, "HP"),
		(ASUS, "Asus"),
		(MICROSOFT, "Microsoft"),
	)
	MODEL_ONE = "9020"
	MODEL_TWO = "9010"
	MODEL_THREE = "780"
	MODEL_FOUR = "790"
	MODEL_FIVE = "CF-30"
	MODEL_SIX = "CF-19"
	MODEL_SEVEN = "Surface Pro 3"
	ALL_MODELS = (
		(MODEL_ONE, "9020"),
		(MODEL_TWO, "9010"),
		(MODEL_THREE, "780"),
		(MODEL_FOUR, "790"),
		(MODEL_FIVE, "CF-30"),
		(MODEL_SIX, "CF-19"),
		(MODEL_SEVEN, "Surface Pro 3"),

	)
	OCME = "OCME"
	SPO = "SPO"
	GRANT = "Grant"
	ALL_OWNERS = (
		(OCME, "OCME"),
		(SPO, "SPO"),
		(GRANT, "Grant"),
	)
	pc_manufacturer = models.CharField(max_length = 32, choices = ALL_MANUFACTURERS, blank = True)
	pc_model = models.CharField(max_length = 32, choices = ALL_MODELS, blank = True)
	pc_host_name = models.CharField(max_length = 64, blank = True)
	pc_service_tag = models.CharField(max_length = 64, blank = True)
	pc_asset_tag = models.CharField(max_length = 64, blank = True)
	pc_owner = models.CharField(max_length = 64, choices = ALL_OWNERS, blank = True)
	pc_user = models.ForeignKey("Employee", related_name = "computers")
	
	def __str__(self):
		return self.pc_manufacturer + " " + self.pc_model + " " + self.pc_host_name + " " + self.pc_service_tag + " " + self.pc_asset_tag

class WirelessDevice(models.Model):
	SMARTPHONE = "Smart Phone"
	MIFI = "MiFi"
	CELLPHONE = "Cell Phone"
	ALL_TYPES = (
		(SMARTPHONE, "Smart Phone"),
		(MIFI, "MiFi"),
		(CELLPHONE, "Cell Phone"),
	)
	SAMSUNG = "Samsung"
	BLACKBERRY = "Blackberry"
	ALL_MANUFACTURERS = (
		(SAMSUNG, "Samsung"),
		(BLACKBERRY, "Blackberry"),
	)
	GALAXY_S_FIVE = "Galaxy S5"
	ALL_MODELS = (
		(BLACKBERRY, "Blackberry"),
		(GALAXY_S_FIVE, "Galaxy S5"),
		(MIFI, "MiFi"),
	)
	VERIZON = "Verizon"
	ATNT = "AT&T"
	SPRINT = "Sprint"
	ALL_CARRIERS = (
		(VERIZON, "Verizon"),
		(ATNT, "AT&T"),
		(SPRINT, "Sprint"),
	)
	wd_type = models.CharField(max_length = 32, choices = ALL_TYPES, blank = True)
	wd_manufacturer = models.CharField(max_length = 32, choices = ALL_MANUFACTURERS, blank = True)
	wd_carrier = models.CharField(max_length = 32, choices = ALL_CARRIERS, blank = True)
	wd_model = models.CharField(max_length = 32, choices = ALL_MODELS, blank = True)
	wd_number = models.CharField(max_length = 64, blank = True)
	wd_serial_number = models.CharField(max_length = 64, blank = True)
	wd_meid = models.CharField(max_length = 64, blank = True)
	wd_asset_tag = models.CharField(max_length = 64, blank = True)
	wd_date_deployed = models.DateField(null = True, blank = True)
	wd_date_returned = models.DateField(null = True, blank = True)
	wd_user = models.ForeignKey("Employee", related_name = "wdevices")

	def __str__(self):
		return self.wd_manufacturer + " " + self.wd_model + " " + self.wd_serial_number + " " + self.wd_asset_tag

class IPPhone(models.Model):
	CISCO = "Cisco"
	ALL_MANUFACTURERS = (
		(CISCO, "Cisco"),
	)
	phone_manufacturer = models.CharField(max_length = 32, choices = ALL_MANUFACTURERS, blank = True)
	phone_model = models.CharField(max_length = 64, blank = True)
	phone_number = models.CharField(max_length = 64, blank = True)
	phone_serial_number = models.CharField(max_length = 64, blank = True)
	phone_mac = models.CharField(max_length = 64, blank = True)
	phone_asset_tag = models.CharField(max_length = 64, blank = True)
	phone_user = models.ForeignKey("Employee", related_name = "phones")

	def __str__(self):
		return self.phone_manufacturer + " " + self.phone_model + " " + self.phone_serial_number + " " + self.phone_asset_tag

class Laptop(models.Model):
	OCME = "OCME"
	SPO = "SPO"
	GRANT = "Grant"
	ALL_OWNERS = (
		(OCME, "OCME"),
		(SPO, "SPO"),
		(GRANT, "Grant"),
	)
	laptop_manufacturer = models.CharField(max_length = 64, blank = True)
	laptop_model = models.CharField(max_length = 64, blank = True)
	laptop_host_name = models.CharField(max_length = 64, blank = True)
	laptop_service_tag = models.CharField(max_length = 64, blank = True)
	laptop_asset_tag = models.CharField(max_length = 64, blank = True)
	laptop_owner = models.CharField(max_length = 32, choices = ALL_OWNERS, blank = True)
	laptop_user = models.ForeignKey("Employee", related_name = "laptops")

	def __str__(self):
		return self.laptop_manufacturer + " " + self.laptop_model + " " + self.laptop_host_name + " " + self.laptop_service_tag + " " + self.laptop_asset_tag

class Printer(models.Model):
	XEROX = "Xerox"
	HP = "HP"
	ZEBRA = "Zebra"
	ALL_MANUFACTURERS = (
		(XEROX, "Xerox"),
		(HP, "HP"),
		(ZEBRA, "Zebra")
	)
	OCME = "OCME"
	SPO = "SPO"
	GRANT = "Grant"
	ALL_OWNERS = (
		(OCME, "OCME"),
		(SPO, "SPO"),
		(GRANT, "Grant"),
	)
	printer_manufacturer = models.CharField(max_length = 32, choices = ALL_MANUFACTURERS, blank = True)
	printer_model = models.CharField(max_length = 64, blank = True)
	printer_serial_number = models.CharField(max_length = 64, blank = True)
	printer_asset_tag = models.CharField(max_length = 64, blank = True)
	printer_owner = models.CharField(max_length = 32, choices = ALL_OWNERS, blank = True)
	printer_user = models.ForeignKey("Employee", related_name = "printers")

	def __str__(self):
		return self.printer_manufacturer + " " + self.printer_model + " " + self.printer_serial_number + " " + self.printer_asset_tag

class BarcodeScanner(models.Model):
	bs_manufacturer = models.CharField(max_length = 64, blank = True)
	bs_model = models.CharField(max_length = 64, blank = True)
	bs_serial_number = models.CharField(max_length = 64, blank = True)
	bs_asset_tag = models.CharField(max_length = 64, blank = True)
	bs_user = models.ForeignKey("Employee", related_name = "bscanners")

	def __str__(self):
		return self.bs_manufacturer + " " + self.bs_model + " " + self.bs_serial_number + " " + self.bs_asset_tag

class BarcodeScannerCradle(models.Model):
	bsc_manufacturer = models.CharField(max_length = 64, blank = True)
	bsc_model = models.CharField(max_length = 64, blank = True)
	bsc_serial_number = models.CharField(max_length = 64, blank = True)
	bsc_asset_tag = models.CharField(max_length = 64, blank = True)
	bsc_user = models.ForeignKey("Employee", related_name = "cradles")

	def __str__(self):
		return self.bsc_manufacturer + " " + self.bsc_model + " " + self.bsc_serial_number + " " + self.bsc_asset_tag

class Scanner(models.Model):
	EPSON = "Epson"
	FUJITSU = "Fujitsu"
	ALL_MANUFACTURERS = (
		(EPSON, "Epson"),
		(FUJITSU, "Fujitsu"),
	)
	DS_FIVE_TEN = "DS-510"
	DS_FIVE_TWENTY = "DS-520"
	V_SEVEN_HUNDRED = "V-700"
	GT_FIFTEEN_HUNDRED = "GT-1500"
	IX_FIVE_HUNDRED = "ix500"
	S_FIVE_HUNDRED = "s500"
	S_FIVE_TEN = "s510"
	S_FIFTEEN_HUNDRED = "s1500"
	ALL_MODELS = (
		(DS_FIVE_TEN, "DS-510"),
		(DS_FIVE_TWENTY, "DS-520"),
		(V_SEVEN_HUNDRED, "V-700"),
		(GT_FIFTEEN_HUNDRED, "GT-1500"),
		(IX_FIVE_HUNDRED, "ix500"),
		(S_FIVE_HUNDRED, "s500"),
		(S_FIVE_TEN, "s510"),
		(S_FIFTEEN_HUNDRED, "s1500"),
	)
	scanner_manufacturer = models.CharField(max_length = 32, choices = ALL_MANUFACTURERS, blank = True)
	scanner_model = models.CharField(max_length = 32, choices = ALL_MODELS, blank = True)
	scanner_serial_number = models.CharField(max_length = 64, blank = True)
	scanner_asset_tag = models.CharField(max_length = 64, blank = True)
	scanner_user = models.ForeignKey("Employee", related_name = "scanners")

	def __str__(self):
		return self.scanner_manufacturer + " " + self.scanner_model + " " + self.scanner_serial_number + " " + self.scanner_asset_tag

class BiometricScanner(models.Model):
	bis_manufacturer = models.CharField(max_length = 64, blank = True)
	bis_model = models.CharField(max_length = 64, blank = True)
	bis_serial_number = models.CharField(max_length = 64, blank = True)
	bis_asset_tag = models.CharField(max_length = 64, blank = True)
	bis_user = models.ForeignKey("Employee", related_name = "bioscanners")

	def __str__(self):
		return self.bis_manufacturer + " " + self.bis_model + " " + self.bis_serial_number + " " + self.bis_asset_tag

class Employee(models.Model):
	DNA = "DNA"
	FIVE_TWENTY = "520"
	BRONX = "Bronx"
	QUEENS = "Queens"
	STATEN_ISLAND = "SI"
	ALL_LOCATIONS = (
		(DNA, "DNA"),
		(FIVE_TWENTY, "520"),
		(BRONX, "Bronx"),
		(QUEENS, "Queens"),
		(STATEN_ISLAND, "SI"),
	)
	last_name = models.CharField(max_length = 64)
	first_name = models.CharField(max_length = 64)
	location = models.CharField(max_length = 32, choices = ALL_LOCATIONS, blank = True)
	department = models.CharField(max_length = 64, blank = True)
	employee_id = models.CharField(max_length = 64, blank = True)
	email = models.CharField(max_length = 64, blank = True)
	is_terminated = models.BooleanField(default = False)

	def __str__(self):
		return self.last_name + ", " + self.first_name

	pass
