from django.contrib import admin
from .models import Employee, Computer, WirelessDevice, IPPhone, Laptop, Printer, BarcodeScanner, BarcodeScannerCradle, Scanner, BiometricScanner 

class ComputerInline(admin.TabularInline):
	extra = 0
	model = Computer

class WDInline(admin.TabularInline):
	extra = 0
	model = WirelessDevice

class IPPhoneInline(admin.TabularInline):
	extra = 0
	model = IPPhone

class LaptopInline(admin.TabularInline):
	extra = 0
	model = Laptop

class PrinterInline(admin.TabularInline):
	extra = 0
	model = Printer

class BarcodeScannerInline(admin.TabularInline):
	extra = 0
	model = BarcodeScanner

class BarcodeScannerCradleInline(admin.TabularInline):
	extra = 0
	model = BarcodeScannerCradle

class ScannerInline(admin.TabularInline):
	extra = 0
	model = Scanner

class BiometricScannerInline(admin.TabularInline):
	extra = 0
	model = BiometricScanner

class EmployeeAdmin(admin.ModelAdmin):
	inlines = [ComputerInline, WDInline, IPPhoneInline, LaptopInline, PrinterInline, BarcodeScannerInline, BarcodeScannerCradleInline, ScannerInline, BiometricScannerInline, ]

class ComputerAdmin(admin.ModelAdmin):
	list_display = ('pc_manufacturer', 'pc_model', 'pc_host_name', 'pc_service_tag', 'pc_asset_tag', 'pc_owner', 'pc_user')

class WDAdmin(admin.ModelAdmin):
	list_display = ('wd_type', 'wd_manufacturer', 'wd_carrier', 'wd_model', 'wd_number', 'wd_serial_number', 'wd_meid', 'wd_asset_tag', 'wd_date_deployed', 'wd_date_returned', 'wd_user')

class PhoneAdmin(admin.ModelAdmin):
	list_display = ('phone_manufacturer', 'phone_model', 'phone_number', 'phone_serial_number', 'phone_mac', 'phone_asset_tag', 'phone_user')

class LaptopAdmin(admin.ModelAdmin):
	list_display = ('laptop_manufacturer', 'laptop_model', 'laptop_host_name', 'laptop_service_tag', 'laptop_asset_tag', 'laptop_owner', 'laptop_user')

class PrinterAdmin(admin.ModelAdmin):
	list_display = ('printer_manufacturer', 'printer_model', 'printer_serial_number', 'printer_asset_tag', 'printer_owner', 'printer_user')

class BSAdmin(admin.ModelAdmin):
	list_display = ('bs_manufacturer', 'bs_model', 'bs_serial_number', 'bs_asset_tag', 'bs_user')

class BSCAdmin(admin.ModelAdmin):
	list_display = ('bsc_manufacturer', 'bsc_model', 'bsc_serial_number', 'bsc_asset_tag', 'bsc_user')

class ScannerAdmin(admin.ModelAdmin):
	list_display = ('scanner_manufacturer', 'scanner_model', 'scanner_serial_number', 'scanner_asset_tag', 'scanner_user')

class BioAdmin(admin.ModelAdmin):
	list_display = ('bis_manufacturer', 'bis_model', 'bis_serial_number', 'bis_asset_tag', 'bis_user')

admin.site.register(Employee, EmployeeAdmin) #Show an employee's devices in admin site with TabularInline inheritance
admin.site.register(Computer, ComputerAdmin)
admin.site.register(WirelessDevice, WDAdmin)
admin.site.register(IPPhone, PhoneAdmin)
admin.site.register(Laptop, LaptopAdmin)
admin.site.register(Printer, PrinterAdmin)
admin.site.register(BarcodeScanner, BSAdmin)
admin.site.register(BarcodeScannerCradle, BSCAdmin)
admin.site.register(Scanner, ScannerAdmin)
admin.site.register(BiometricScanner, BioAdmin)
