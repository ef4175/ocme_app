Use
	Admin Page
		Get page by appending "/admin" to base URL
		Log in as a superuser (default username and password: ocmeadmin, ocmemaster)
		Add user to system by clicking on "Users" link and "Add user" on upper-right. This is intentionally the only way to register users.
		Modify account details or permissions by clicking on a username and selecting corresponding boxes as needed
		It is possible to register devices on this admin page, although this is ill-advised because you may inadverdently put duplicates in the database or mismanage device ownership
	User Page
		User authentication is required for access
		You will automatically be logged out if either you are inactive for more than 15 minutes or you close your web browser (not the tab). You also can log out manually.
		Every table on the user page has a bit of a hidden feature; clicking on any column header, denoted by a bold font, will sort all entries under that column in alphabetically ascending order. Clicking again will sort in alphabetically descending order.

		Important: a "root employee" named OCME, OCME is a legitimate instance of the Employee model. This is the employee whose device lists you should use if a device will be registered in the database but is not owned or given to an employee at this agency. Every employee is assigned a unique ID (analogous to an Excel file in which each employee is put on a different row; the row number is the ID for each employee), and the OCME, OCME root employee must have an ID of 1 so that device ownership is maintained properly. See maintenance section for more information regarding database wipes.

		The home page will show a navigation container for lists of common attributes in the database. The "Departments" hyperlink will display all departments that are in the database. Similarly, "Locations" will show all locations and their departments. Click on a department name to get a list of every employee in that department. Underlined employee names connote a hyperlink to their profile pages, where you can modify any information.

		Add a new employee by clicking the "Add" button the upper-right. Register devices for this new employee with the panel on the right. Data entry should be self-explanatory, though there are some nuances that should be articulated:
			When adding a new employee and assigning a wireless device, please do not register dates (as prompted explicitly on the wireless device form) here. Register dates when you are redirected to the employee's profile page by editing the information of a wireless device.
			Clicking the delete button for any form will delete the form and clear any data that was entered. This will not affect the database at all.
			This web app is intended to track ownership of devices. If the "root employee," the alias for all unowned devices, has a device and you add a device for an actual employee, the database will be checked for redundancies and adjusted accordingly. For example, if the root employee is holding a Dell 9020 with a particular identifier (a combination of host name, service tag, and asset tag), and this exact information is entered for a new PC for a new employee, this Dell 9020 will be "taken" from the root employee and "given" to the new employee. If there are no matches in the database, information for a new device will be registered.

		Upon submitting the 'add employee' form, you will be redirected to the employee's profile page. Here, you can click 'Edit' on the upper-right to modify personal information (in the event that you made a typo or, say, this employee is moved to a different location or department). You also can add devices for an employee on his/her profile page. If you do this, the database again will be checked for redundancies; read above. Each device associated with this employee has three hyperlinks, which should be self-explanatory. Returning a device will "remove" the device from an employee and be "given" to the root employee. Editing device information is straightforward, but be mindful that the database will again be checked for redundancies, as described above. Deleting a device will completely erase its entry from the database, not return it to OCME.

		Every actual employee will have a red terminate button on his/her profile page. Clicking and confirming will not wipe his/her personal information for archival purposes. Instead, every device owned by this employee will be returned to OCME and no new device can be given to him/her.

		There is a search button in the header. This is an all-inclusive search query, in that every single entry in the database will be checked if the search string is a substring of any entry. For instance, if you search for "a," everything in the database that contains the letter a will be retrieved. Similarly, if you search for "9001," which is common practice for querying PC hostnames, every entry in the database will be checked if it contains "9001" anywhere, so it will retrieve both "8900156" and "1269001." This search function is case-insensitive; searching for "ONE," "onE," or "one" will return the same results. Efficiency should not be a concern; Django, with which this web app was built, takes just 8 milliseconds to scan a list of 60,000 e-mails. 

		As with most websites, click on the logo to return to the home page.

Maintenance
	This web app can be cloned from: https://github.com/ef4166/ocme_app. If you use the Git version-control program, cd into a directory of choice and type "git clone https://github.com/ef4166/ocme_app.git" in the command line. Alternatively, you can visit the website and download a zipped file of this web app. 

	This program is entirely dependent on having Python 3.4.2 (other versions may suffice) installed on a machine because Python scripts must be executed. Follow the instructions on Django's official website for setup.

	In the event that a database wipe is essential, please re-create the OCME, OCME root employee by creating a superuser and adding this employee through the admin site only. Ensure that its ID is 1 by creating a test employee and terminating it; all devices owned by this test employee should go back to the root employee.

	Drop-down menus will inevitably have to be modified as technology advances and new models are purchased. The values of drop-down menus can be changed by editing the models.py file. Navigate to the class you intend to modify and add enum types to the tuples you want to change. 

	At some point in the future, if you decide to replicate this web app using technologies with which the IT department is more familiar, the most critical file you should keep is the database file, which can be found in the root directory and is aptly named "db." Do note that this database is maintained with SQLite, so some hacking may be needed to convert it to a database library that the department typically uses.